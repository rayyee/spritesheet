package
{
	import com.bit101.components.PushButton;
	
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.net.FileFilter;
	import flash.net.SharedObject;
	
	import cc.AnimLoader;
	
	import fw.ResourceManager;
	
	import model.AppModel;
	
	import ui.CharAnimView;
	
	public class AnimTool extends Sprite
	{
		private var _appModel:AppModel;
		private var _rm:ResourceManager;
		
		private var _view:CharAnimView;
		
		public function AnimTool()
		{
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
		}
		
		
		protected function onAdded(event:Event):void
		{
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.frameRate= 60;
			
			var so:SharedObject = SharedObject.getLocal("userData");
			if (so.data.winX) {
				stage.nativeWindow.x = so.data.winX;
				stage.nativeWindow.y = so.data.winY;
			}
			
			var appWidth:Number= 904;
			graphics.beginFill(0x434E8E);
			graphics.drawRect(0,0, appWidth, 24);
			graphics.endFill();
			
			graphics.beginFill(0xf0f0f0);
			graphics.drawRect(0,24, appWidth, 600);
			graphics.endFill();
			
		
			
			var btn:PushButton = new PushButton(this, 30, 2, "Open", onClickOpen);
			btn.width= 40; btn.height=18;
			
			// TODO: checkers pattern
			
			_rm = new ResourceManager();
			
			_appModel= new AppModel();
			
			_view = new CharAnimView(_appModel);
			_view.y = 30;
			addChild(_view);
			
			animLoader = new AnimLoader(_appModel);
			
		}
		
		private function onClickOpen(e:Event):void
		{
			var so:SharedObject = SharedObject.getLocal("userData");
			var last:String;
			
			var fb:File= File.documentsDirectory;
			if (so.data) {
				last = so.data.lastPath;
			}
			
			trace("last= " + last);
			
			if (last) {
				fb=	fb.resolvePath(last);
			}
			
			fb.addEventListener(Event.SELECT, onFileSelect);
			//fb.browseForDirectory("Select character folder..");
			var txtFilter:FileFilter = new FileFilter("JSON", "*.json");
			
			fb.browseForOpen("Select Descriptor file", [txtFilter]);
		}		
		
		private var animLoader:AnimLoader;
		
		protected function onFileSelect(event:Event):void
		{
			var dataPath:String = event.target.url;
			
			animLoader.loadAnimParts(dataPath, initCharacter);
			
			var so:SharedObject = SharedObject.getLocal("userData");
			so.data.lastPath = dataPath;
			so.data.winX = stage.nativeWindow.x;
			so.data.winY = stage.nativeWindow.y;
			so.flush();
		}
		
		private function initCharacter(d:Array):void
		{
			trace("initCharacter");
			
			_view.initAnim(animLoader.createAnimSprite());
		}
		
	}
	
}