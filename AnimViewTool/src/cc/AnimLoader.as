package cc
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.filesystem.File;
	import flash.geom.Point;
	
	import fw.ResourceManager;
	import fw.anim.AnimSprite;
	import fw.anim.AnimTextureSheet;
	import fw.anim.TileSheetHelper;
	import fw.utils.loading.BigLoader;
	
	import model.AppModel;
	
	
	// Helper class to load a multipart charater animation
	// - Load the descriptor file and the associated assets
	// - Create the individual parts that make up the character
	
	public class AnimLoader
	{
		private var _appModel:AppModel;
		protected var _dataLoader:BigLoader;
		
		private var _respCb:Function;
		private var _baseURL:String;
		private var _descData:Object;
		private var _texSheet:AnimTextureSheet;
		private var _starlingFormat:Boolean;
		
		private var helper:TileSheetHelper;
		
	
		
		public function AnimLoader(model:AppModel)
		{
			_appModel= model;
			
			helper = new TileSheetHelper();
		}
		
		
		public function dispose():void
		{
			
			
			_dataLoader.removeEventListener(Event.COMPLETE, onDataLoaded);
			
			
			_dataLoader= null;
			_respCb= null;
		}
		
		
		public function loadAnimParts(path:String, responder:Function):void
		{
			_respCb= responder;
			_dataLoader= new BigLoader();
			
			loadDescriptor(path);
		}
		
		private function loadDescriptor(dataUrl:String):void
		{
			var endp:int= dataUrl.lastIndexOf("/");
			if (endp < 0) endp=  dataUrl.lastIndexOf("\\");
			_baseURL= dataUrl.substring(0, endp+1);
			
			_dataLoader.reset();
			
			_dataLoader.add( dataUrl, "desc");
			_dataLoader.addEventListener(Event.COMPLETE, onDataLoaded);
			_dataLoader.addEventListener(IOErrorEvent.IO_ERROR, onLoadFailed);
			_dataLoader.start();	
			
		}
		
		protected function onLoadFailed(event:IOErrorEvent):void
		{
			//log("Cannot find parts.xml !");
			log(event.text);
		}
		
		private function log(msg:String):void
		{
			_appModel.log( msg);
		}
		
		
		private var assetId:String;
		
		protected function onDataLoaded(event:Event):void
		{
			var rawJson:String = _dataLoader.getLoadedAssetById("desc");
			var dataAssetName:String;
			_descData = JSON.parse(rawJson);
				
			assetId = _descData.base;
			_starlingFormat= false;
			
			// Decide if we are reading json or xml format
			var dataFile:File = new File(_baseURL+ assetId + ".json");
			if (dataFile.exists) {
				dataAssetName = assetId + ".json";
			}
			else {
				dataAssetName = assetId + ".xml";
				_starlingFormat= true;
			}
			
			_dataLoader.removeEventListener(Event.COMPLETE, onDataLoaded);
			_dataLoader.addEventListener(Event.COMPLETE, onAssetsLoaded);
			
			_dataLoader.reset();
			_dataLoader.add( _baseURL+ assetId + ".png", "sheetBmp");
			_dataLoader.add( _baseURL+ dataAssetName, "sheetData");
			// Load the assets
			log("Loading assets...");
			_dataLoader.start();
		}		
		
		protected function onAssetsLoaded(event:Event):void
		{
			log("All parts loaded.");
			_respCb(null);
		}
		
	
		
		private function onLoadError(msg:String):void
		{
			log(msg);
		}
		
		// Usage: call this function after all assets have been loaded
		//
		public function createAnimSprite():AnimSprite
		{
			var anim:AnimSprite = new AnimSprite();
			
			var sheetImg:Bitmap = _dataLoader.getLoadedAssetById("sheetBmp");
			_texSheet= helper.prepareAnimTexture(sheetImg.bitmapData, _dataLoader.getLoadedAssetById("sheetData"), _starlingFormat);
			anim.initialize(_texSheet);
			
			// Get the sequence list from the descriptor
			var seqList:Array = _descData.seqList;
			var seqInfo:Object;
			var arFrames:Array;
			
			// Add each sequence into the anim-sprite
			for (var i:int = 0; i < seqList.length; i++) {
				seqInfo = seqList[i];
				arFrames = parseArray(seqInfo.frames, true);
				
				anim.addSequence(seqInfo.name, arFrames, 60, seqInfo.loop);
			}
			anim.forceFrameRate(60);
		
			log("Animation is ready.");
			
			
			return anim;
		}
		
		// Turn cap first into camel case
		private function fixCase(src:String):String
		{
			return (src.substr(0,1).toLowerCase() + src.substr(1));
		}
		
		public static function createSeqArray(count:int):Array
		{
			var arOut:Array= []; // of int
			
			for (var j:int = 0; j < count; j++) {
				arOut.push(j);
			}
			
			return arOut;
		}
		
		
		// Parse array from xml
		// Returns an array of int
		//
		private function parseArray(src:String, oneBased:Boolean=false):Array
		{
			// commas
			var arData:Array= src.split(",");
			var arOut:Array= []; // of int
			var index:int;
			
			for (var i:int = 0; i < arData.length; i++) {
				var elem:String = arData[i];
				
				if (elem.indexOf("-") > 0) {
					
					var r0:int = int(elem.split("-")[0]);
					var r1:int = int(elem.split("-")[1]);
					if (oneBased) {
						--r0; --r1;
					}
					for (var j:int = r0; j <=r1; j++) {
						arOut.push(j);
					}
				}
				else {
					index= int(elem);
					if (oneBased) --index;
					arOut.push(index);
				}
			}
			
			return arOut;
		}
		
		
	}
}
