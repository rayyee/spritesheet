package ui
{
	import flash.display.DisplayObject;
	import flash.display.SimpleButton;
	
	public class BitmapButton extends SimpleButton
	{
		public function BitmapButton(upState:DisplayObject=null, overState:DisplayObject=null, downState:DisplayObject=null, hitTestState:DisplayObject=null)
		{
			super(upState, overState, downState, hitTestState);
		}
	}
}