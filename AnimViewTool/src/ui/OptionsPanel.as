package ui
{
	import com.bit101.components.CheckBox;
	import com.bit101.components.ComboBox;
	import com.bit101.components.Component;
	import com.bit101.components.HBox;
	import com.bit101.components.Label;
	import com.bit101.components.NumericStepper;
	import com.bit101.components.PushButton;
	import com.bit101.components.Slider;
	import com.bit101.components.TextArea;
	import com.bit101.components.VBox;
	import com.bit101.components.Window;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObjectContainer;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import fw.anim.AnimSprite;
	
	import model.AppModel;
	
	public class OptionsPanel extends Sprite
	{
		private var _appModel:AppModel;
		private var _view:CharAnimView;
		private var _charAnim:AnimSprite;
		
		private var arBoxItems:Array;
		private static const PANEL_WIDTH:Number= 330;
		
		
		private var cbBoxes:CheckBox;
		private var cbMainBox:CheckBox;
		private var lblFrames:Label;
		private var seqCombo:ComboBox;
		private var cbZoom:CheckBox;
		private var sldZoom:Slider;
		private var lblZoom:Label;
		private var sldFPS:Slider;
		private var lblFPS:Label;
		
		private var arVisibleCb:Array;
		private var lblNumFrames:Label;
		private var cbLoop:CheckBox;
		private var lblAnimFrame:TextField;
		private var curFrame:int;
		private var seqNumFrames:int;
		
		public function OptionsPanel(model:AppModel)
		{
			super();
			
			_appModel= model;
			arBoxItems= [];
			arVisibleCb= [];
		}
		
		private function extractName(src:String):String
		{
			var end:int = src.indexOf("_");
			return src.substring(0, end);
		}
		
		public function get numFramesInSeq():int
		{
			var seq:int= seqCombo.selectedIndex;
			
			return _charAnim.getSequenceData(seq).arFrames.length;
		}
		
		public function advanceFrame():void
		{
			_charAnim.frameAdvance(true);
		}
		
		public function setInfo(numFrames:int, char:AnimSprite):void
		{
			_charAnim= char;
			lblFrames.text= numFrames.toFixed();
			curFrame=0;
					
			for (var i:int = 0; i < char.numSequences; i++) {
				seqCombo.addItem(char.getSequence(i));
			}
			seqCombo.selectedIndex=0;
			
		}
		
		public function updateFrame():void
		{
			if (curFrame == _charAnim.seqFrame) return;
			
			curFrame = _charAnim.seqFrame;
			lblAnimFrame.text= curFrame.toString();
		}
		
		// Create GUI
		public function createGUI(view:CharAnimView):void
		{
			_view = view;
			
			var dispPanel:Window = addPanel(1,2, 160, "Character Display");
			var animPanel:Window = addPanel(1,168, 160, "Animate");
			var logPanel:Window= addPanel(1,332, 200, "Log");
				
			populateDisplayPanel(dispPanel);
			
			populateAnimPanel(animPanel);
			
			var log:TextArea= new TextArea(logPanel, 4,6, "---- System Log ------\n\n");
			log.width= PANEL_WIDTH-10; log.height = logPanel.height-28;
			log.editable= false;
			log.textField.wordWrap= false;
			
			_appModel.setLogControl(log);
			
		}
		
		[Embed(source="PlayBtn.png")]
		private static var PlayBtn:Class;
		
		[Embed(source="PlusBtn.png")]
		private static var PlusBtn:Class;
		
		[Embed(source="PrevBtn.png")]
		private static var MinusBtn:Class;
		
		
		
		private function addButton(pnl:DisplayObjectContainer, xpos:Number, ypos:Number, img:Bitmap, 
								   cb:Function=null, id:String=null):void
		{
			var btn:UIButton = new UIButton(img.bitmapData);
			btn.x = xpos; btn.y = ypos;
			pnl.addChild(btn);
			
			if (cb != null) {
				btn.clicked.add(cb);
			}
			
			if (id) {
				btn.name = id;
			}
		}
		
		private function populateAnimPanel(pnl:Component):void
		{
			new Label(pnl, 12,12, "Total Frames:");
			lblFrames= new Label(pnl, 100,12, "----");
			
			new Label(pnl, 12,40, "Sequence:");
			seqCombo = new ComboBox(pnl, 82, 40);
			seqCombo.width= 126;
			seqCombo.addEventListener(Event.SELECT, onSelectSequnce);
			
			lblNumFrames = new Label(pnl, 222,40, "---");
			new Label(pnl, 240, 40, "Frames");
			
			
			addButton(pnl,  16, 82, new PlayBtn(), onPlay);
			addButton(pnl,  160, 82, new MinusBtn(), onFrameAdvance, "prev");
			addButton(pnl,  210, 82, new PlusBtn(), onFrameAdvance, "next");
			
			var rcShape:Shape = new Shape();
			rcShape.graphics.clear();
			rcShape.graphics.lineStyle(1, 0x999999);
			rcShape.graphics.beginFill(0xFFF5C6);
			rcShape.graphics.drawRect(0,0, 42, 34);
			rcShape.graphics.endFill();
			
			pnl.addChild(rcShape);
			rcShape.x = 112; rcShape.y = 83;
			
			// label
			lblAnimFrame= new TextField();
			lblAnimFrame.x = 108; lblAnimFrame.y = 85;
			lblAnimFrame.width= 42; lblAnimFrame.height= 30;
			lblAnimFrame.text= "00";
			lblAnimFrame.autoSize= "right";
			pnl.addChild(lblAnimFrame);
			
			var fmt:TextFormat= new TextFormat("_sans", 26, 0x888888, true);
			lblAnimFrame.setTextFormat(fmt);
			lblAnimFrame.defaultTextFormat= fmt;
			
			var btnExp:PushButton= new PushButton(pnl, 210, 120, "Export Seq", onExport);
			btnExp.width= 90; 
			btnExp.name= "export";
				
		}
		
		private function onPlay(id:String):void
		{
			var ended:Boolean =  curFrame == seqNumFrames-1;
			if (!_charAnim) return;
			
			if (!_charAnim.isPlaying()) {
				if (ended) _charAnim.play(String(seqCombo.selectedItem)); // play from start
				else _charAnim.play(); // resume play
			}
			else {
				_charAnim.stop();
			}
		}
		
		private function onFrameAdvance(id:String):void
		{
			_charAnim.frameAdvance(id== "next");
		
		}
		
		private function onExport(e:Event):void
		{
			//trace("export seq:" + String(seqCombo.selectedItem));
			
			
			var fb:File= File.documentsDirectory;
			fb.addEventListener(Event.SELECT, onExportFileSelect);
			fb.browseForSave("Export to");
		}
		
		protected function onExportFileSelect(event:Event):void
		{
			var dataPath:String = event.target.nativePath;
			var seq:String= String(seqCombo.selectedItem);
			
			_view.exportSeq(seq, dataPath);
		}
		
		private function populateDisplayPanel(pnl:Component):void
		{	
			var border:Shape= new Shape();
			pnl.addChild(border);
			border.graphics.lineStyle(1, 0xa2a2a2);
			border.graphics.drawRect(8, 12, pnl.width-16, 50);
			
			border.graphics.lineStyle(0, 0, 0);
			border.graphics.beginFill(0xf0f0f0);
			border.graphics.drawRect(12, 6, 88, 8);
			border.graphics.endFill();
			new Label(pnl, 12,2, "Parts Visibility");
			
			cbMainBox= new CheckBox(pnl, 16, 30, "Show Character Bounds", onBBox);
			cbMainBox.selected= true;

			
			new Label(pnl, 8,80, "Frame Rate:");
			lblFPS= new Label(pnl, 90,80, "60");
			sldFPS= addSlider(pnl, 118, 83, onFPS, 1, 60, 2);
			sldFPS.value= 60;
			
			new Label(pnl, 12,110, "Zoom:");
			lblZoom= new Label(pnl, 74,110, "100%");
			sldZoom= addSlider(pnl, 118, 113, onZoom, 1, 4, 0.1);
			
		}
		
		private function addSlider(pnl:Sprite, xp:Number, yp:Number, handlerFn:Function, rMin:Number, rMax:Number, rTick:Number):Slider
		{
			var sldr:Slider= new Slider(Slider.HORIZONTAL, pnl, xp, yp, handlerFn);
			sldr.minimum= rMin; sldr.maximum= rMax; 
			sldr.tick = rTick;
			sldr.width= 200; sldr.height= 14;
			
			return sldr;
		}
		

		
		private function onZoom(e:Event):void
		{
			lblZoom.text = (100*sldZoom.value).toFixed() + '%';
			_view.scaleX= sldZoom.value;
			_view.scaleY= sldZoom.value;
			
			// center
			_view.centerView();
			
			
		}
		
		private function onFPS(e:Event):void
		{
			lblFPS.text = (sldFPS.value).toFixed();
			_view.setFrameRate(int(sldFPS.value));
		}
		
		protected function onSelectSequnce(event:Event):void
		{
			trace("seq: " + seqCombo.selectedItem);
			var seq:int= seqCombo.selectedIndex;
			
			
			seqNumFrames= _charAnim.getSequenceData(seq).arFrames.length;
			
			lblNumFrames.text= seqNumFrames.toFixed();
			
			_charAnim.play(String(seqCombo.selectedItem));
		}
		
		private function onBBox(e:Event):void
		{
			//trace("show box = " + cbBoxes.selected);
			_view.drawBounds(cbMainBox.selected);
		}
		
		
	
		
		private function addPanel(px:Number, py:Number, ph:Number, title:String, wid:Number=PANEL_WIDTH):Window
		{
			var aPanel:Window = new Window(this,px, py, title);
			aPanel.width= wid;
			aPanel.height= ph;
			aPanel.barColor = 0xccccdf;
			aPanel.draggable= false;
			
			return aPanel;
		}
		
		
		
		private function addStepper(parent:Component,min:Number, max:Number, label:String):NumericStepper
		{
			var lbl:Label= new Label();
			lbl.text= label;
			parent.addChild(lbl);
			
			var r1:NumericStepper = new NumericStepper(null, 0,0);
			
			r1.width= 60;
			r1.minimum= min;
			r1.maximum = max;
			
			parent.addChild(r1);
			
			return r1;
		}
	}
}
